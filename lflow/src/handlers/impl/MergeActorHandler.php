<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\handlers\impl;

/**
 * actor all方式的合并处理器
 *
 * @author Mr.April
 * @since  1.0
 */
class MergeActorHandler extends AbstractMergeHandler
{

    /**
     * 调用者需要提供actor all的任务名称
     */
    private string $taskName;

    /**
     * 构造函数，由调用者提供taskName
     *
     * @param string $taskName
     */
    public function __construct(string $taskName)
    {
        $this->taskName = $taskName;
    }

    protected function findActiveNodes(): string
    {
        return $this->taskName;
    }
}
