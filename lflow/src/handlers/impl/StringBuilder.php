<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\handlers\impl;
/**
 *
 * 字符串处理器
 * @author Mr.April
 * @since  1.0
 */
class StringBuilder
{
    const LINE = "<br/>";
    protected array $list = [];

    public function __construct($str = NULL)
    {
        if (empty($str)) {
            $this->list = [];
        } else {
            $this->list[] = $str;
        }
    }

    public function append($str): StringBuilder
    {
        $this->list[] = $str;
        return $this;
    }

    public function appendLine($str): StringBuilder
    {
        $this->list[] = $str . self::LINE;
        return $this;
    }

    public function appendFormat($str, mixed $args): StringBuilder
    {
        $this->list[] = sprintf($str, $args);
        return $this;
    }

    public function toString(): string
    {
        return implode(",", $this->list);
    }

    public function __destruct()
    {
        unset($this->list);
    }
}