<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace lflow\dao;

use lflow\model\HistoryOrderModel;
use lflow\model\OrderModel;

/**
 * @author Mr.April
 * @since  1.0
 */
class HistoryOrderDao extends BaseDao
{
    protected function setModel(): string
    {
        return HistoryOrderModel::class;
    }

    /**
     * 通过OrderModel生成HistoryOrder模型
     *
     * @param \lflow\model\OrderModel $orderModel
     *
     * @return \lflow\model\HistoryOrderModel
     */
    public function historyOrder(OrderModel $orderModel): HistoryOrderModel
    {
        $model = new HistoryOrderModel();
        $model->set('id', $orderModel->getData('id'));
        $model->set('process_id', $orderModel->getData('process_id'));
        $model->set('create_time', $orderModel->getData('create_time'));
        $model->set('expire_time', $orderModel->getData('expire_time'));
        $model->set('creator', $orderModel->getData('creator'));
        $model->set('parent_id', $orderModel->getData('parent_id'));
        $model->set('priority', $orderModel->getData('priority'));
        $model->set('order_no', $orderModel->getData('order_no'));
        $model->set('variable', $orderModel->getData('variable'));
        return $model;
    }

    /**
     * 查询列表
     *
     * @param array  $where
     * @param string $field
     * @param int    $page
     * @param int    $limit
     * @param string $order
     * @param array  $with
     * @param bool   $search
     *
     * @return \think\Collection
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getOrders(array $where, string $field = '*', int $page = 0, int $limit = 0, string $order = '', array $with = [], bool $search = false): \think\Collection
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->field($field)->when($page && $limit, function ($query) use ($page, $limit) {
            $query->page($page, $limit);
        })->when($order !== '', function ($query) use ($order) {
            $query->order($order);
        })->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->select();
    }

    /**
     * 查询列表Count
     *
     * @param array $where
     * @param array $with
     * @param bool  $search
     *
     * @return int
     * @throws \ReflectionException
     * @throws \think\db\exception\DbException
     */
    public function getOrdersCount(array $where, array $with = [], bool $search = false): int
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->count();
    }

    /**
     * 获取详情
     *
     * @param array  $where
     * @param string $field
     * @param array  $with
     * @param bool   $search
     *
     * @return mixed
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getInfo(array $where, string $field = "*", array $with = [], bool $search = false): mixed
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->field($field)->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->find();
    }
}
