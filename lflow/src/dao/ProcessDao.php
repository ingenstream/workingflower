<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace lflow\dao;

use lflow\dao\BaseDao;
use lflow\model\ProcessModel;

/**
 * @author Mr.April
 * @since  1.0
 */
class ProcessDao extends BaseDao
{
    protected function setModel(): string
    {
        return ProcessModel::class;
    }

    public function getLatestProcessVersion(string $name){
         $this->setModel()->getMax($where,'version');
    }
}
