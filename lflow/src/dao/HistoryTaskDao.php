<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace lflow\dao;

use lflow\dao\BaseDao;
use lflow\model\HistoryTaskModel;
use lflow\model\TaskModel;

/**
 * @author Mr.April
 * @since  1.0
 */
class HistoryTaskDao extends BaseDao
{
    protected function setModel(): string
    {
        return HistoryTaskModel::class;
    }

    public function getModel(): \lflow\model\BaseModel
    {
        return parent::getModel();
    }

    /**
     * 通过TaskModel生成历史模型
     *
     * @param \lflow\model\TaskModel $taskModel
     *
     * @return \lflow\model\HistoryTaskModel
     */
    public function historyTask(TaskModel $taskModel): HistoryTaskModel
    {
        $model = $this->getModel();
        $model->set('id', $taskModel->getData('id') ?? '');
        $model->set('process_id', $taskModel->getData('process_id') ?? '');
        $model->set('order_id', $taskModel->getData('order_id') ?? '');
        $model->set('create_time', $taskModel->getData('create_time') ?? '');
        $model->set('display_name', $taskModel->getData('display_name') ?? '');
        $model->set('task_name', $taskModel->getData('task_name') ?? '');
        $model->set('task_type', $taskModel->getData('task_type') ?? '');
        $model->set('expire_time', $taskModel->getData('expire_time') ?? '');
        $model->set('action_url', $taskModel->getData('action_url') ?? '');
        $model->set('parent_task_id', $taskModel->getData('parent_task_id') ?? '');
        $model->set('variable', $taskModel->getData('variable') ?? (object)[]);
        $model->set('perform_type', $taskModel->getData('perform_type') ?? '');
        return $model;
    }

    /**
     * 查询任务列表
     *
     * @param array  $where
     * @param string $field
     * @param int    $page
     * @param int    $limit
     * @param string $order
     * @param array  $with
     * @param bool   $search
     *
     * @return \think\Collection
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getHistoryTask(array $where, string $field = '*', int $page = 0, int $limit = 0, string $order = '', array $with = [], bool $search = false): \think\Collection
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->field($field)->when($page && $limit, function ($query) use ($page, $limit) {
            $query->page($page, $limit);
        })->when($order !== '', function ($query) use ($order) {
            $query->order($query->getTable() . '.' . $order);
        })->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->distinct(true)->select();
    }

    /**
     * 查询任务列表Count
     *
     * @param array $where
     * @param array $with
     * @param bool  $search
     *
     * @return int
     * @throws \ReflectionException
     * @throws \think\db\exception\DbException
     */
    public function getHistoryTaskCount(array $where, array $with = [], bool $search = false): int
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->distinct(true)->count();
    }

    /**
     * 获取详情
     *
     * @param array  $where
     * @param string $field
     * @param array  $with
     * @param bool   $search
     *
     * @return array|mixed
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getInfo(array $where, string $field = "*", array $with = [], bool $search = false): mixed
    {
        if ($search) {
            $model = $this->search($where);
        } else {
            $model = $this->getModel()->where($where);
        }
        return $model->field($field)->when($with, function ($query) use ($with) {
            $query->withJoin($with);
        })->find();
    }

}
