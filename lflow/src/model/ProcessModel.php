<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\model;

use lflow\ckpt\ProcessCkpt;
use lflow\lib\util\Str;
use think\db\Query;

class ProcessModel extends BaseModel
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     *
     * @var string
     */
    protected $name = 'wf_process';

    protected array $insert = ['create_time'];

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'update_time' => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * @var \lflow\ckpt\ProcessCkpt
     */
    public ProcessCkpt $process_ckpt;

    /**
     * JSON字段
     *
     * @var string[]
     */
    protected $json = ['content'];

    /**
     * 新增自动创建字符串id
     *
     * @param $model
     *
     * @return void
     */
    protected static function onBeforeInsert($model): void
    {
        $uuid                = !empty($model->{$model->pk}) ? $model->{$model->pk} : Str::uuid();
        $model->{$model->pk} = $uuid;
    }

    /**
     * ID搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 类型搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchTypeAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('type', $value);
        }
    }

    /**
     * 流程名称搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchNameAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('name', $value);
        }
    }

    /**
     * 显示名称搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchDisplayNameAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('display_name', $value . '%');
        }
    }

    /**
     * 实例URL搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchInstanceUrlAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('instance_url', $value . '%');
        }
    }

    /**
     * 流程状态搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchStateAttr(Query $query, $value)
    {
        if ($value !== '') {
            $query->where('state', $value);
        }
    }

    /**
     * 流程版本搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchVersionAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('version', $value);
        }
    }

    /**
     * 流程创建人搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchCreatorAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('creator', $value);
        }
    }

}
