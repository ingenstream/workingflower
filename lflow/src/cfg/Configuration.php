<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\cfg;

use lflow\impl\LogInterceptor;
use lflow\impl\SchedulerInterceptor;
use lflow\impl\SurrogateInterceptor;
use lflow\lib\util\ClassUtil;
use lflow\lib\util\ObjectHelper;
use ReflectionClass;

class Configuration
{
    /**
     * 拦截配置
     *
     * @var array
     */
    private array $interceptor = [
//        LogInterceptor::class,
        SurrogateInterceptor::class,
    ];

    public function __construct()
    {
        //类装载服务
        foreach ($this->interceptor as $interceptor) {
            if (class_exists($interceptor)) {
                app()->make($interceptor);
            }
        }
    }


    /**
     * 根据接口获取类列表
     *
     * @param string $clazz
     *
     * @return array
     */
    public function interceptList(string $clazz): array
    {
        try {
            $classes   = $this->interceptor;
            $clazzList = [];
            foreach ($classes as $klass) {
                $reflect = new ReflectionClass($klass);
                if (interface_exists($clazz) && $reflect->implementsInterface($clazz)) {
                    $clazzList[] = ClassUtil::instantiateClass($klass);
                }
            }
            return $clazzList;
        } catch (\Exception $e) {
            return [];
        }
    }
}
