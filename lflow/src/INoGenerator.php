<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow;

use lflow\ckpt\ProcessCkpt;


/**
 * 编号生成器接口
 * 流程实例的编号字段使用该接口实现类来产生对应的编号
 *
 * @author Mr.April
 * @since  1.0
 */
interface INoGenerator
{
    /**
     *  生成器方法
     *
     * @param \lflow\ckpt\ProcessCkpt $ckpt
     *
     * @return string  编号
     */
    public function generate(ProcessCkpt $ckpt): string;

}
