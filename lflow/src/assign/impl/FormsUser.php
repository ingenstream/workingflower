<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign\impl;


use lflow\assign\Assignment;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;

/**
 * 表单指定用户
 *
 * @author Mr.April
 * @since  1.0
 */
class FormsUser extends Assignment
{
    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {
        //通过提交表单字段对应Key 获取用户id 并return
        return '';
    }

}
