<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign\impl;

use lflow\assign\Assignment;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;
use lflow\lib\util\AssertHelper;

/**
 * 申请人处理
 *
 * @author Mr.April
 * @since  1.0
 */
class Proposer extends Assignment
{

    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {
        $order = $execution->getOrder();
        AssertHelper::notNull($order, '【异常实例不存在或被删除】');
        $actors = $order->getData('creator');
        AssertHelper::notNull($actors, '用户不存在或已离职]');
        return $actors;
    }
}
