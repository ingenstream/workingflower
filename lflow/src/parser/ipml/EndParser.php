<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\EndCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\parser\AbstractNodeParser;

/**
 * 结束节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class EndParser extends AbstractNodeParser
{

    /**
     * 产生EndCkpt模型对象
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new EndCkpt();
    }

}
