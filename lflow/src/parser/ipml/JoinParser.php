<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\JoinCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\parser\AbstractNodeParser;

/**
 * 合并节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class JoinParser extends AbstractNodeParser
{

    protected function newCkpt():NodeCkpt
    {
        return new JoinCkpt();
    }

}
