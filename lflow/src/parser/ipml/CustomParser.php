<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\CustomCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\lib\util\ObjectHelper;
use lflow\parser\AbstractNodeParser;
use lflow\parser\NodeParser;

/**
 * 自定义节点解析器
 *
 * @author Mr.April
 * @since  1.0
 */
class CustomParser extends AbstractNodeParser
{

    /**
     * 由于自定义节点需要解析attr属性，这里覆盖抽象类方法实现
     *
     * @param \lflow\ckpt\NodeCkpt $ckpt
     * @param object|array|null    $nodes
     */
    protected function parseNode(NodeCkpt $ckpt, object|array|null $nodes): void
    {
        if ($ckpt instanceof CustomCkpt) {
            $properties = ObjectHelper::getObjectValue($nodes, NodeParser::ATTR_PROPERTIES) ?? (object)[];
            $ckpt->setClazz(ObjectHelper::getObjectValue($properties, NodeParser::ATTR_CLAZZ));
            $ckpt->setMethodName(ObjectHelper::getObjectValue($properties, NodeParser::ATTR_METHODNAME));
            $ckpt->setArgs(ObjectHelper::getObjectValue($properties, NodeParser::ATTR_ARGS));
            $ckpt->setVar(ObjectHelper::getObjectValue($properties, NodeParser::ATTR_VAR));
        }
    }

    /**
     * 产生CustomCkpt模型
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new CustomCkpt();
    }

}
