<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser;

use lflow\ckpt\NodeCkpt;
use lflow\model\ProcessModel;

/**
 * 节点解析接口
 *
 * @author Mr.April
 * @since  1.0
 */
interface NodeParser
{

    /**
     * 变迁节点名称
     */
    const NODE_TRANSITION = "transition";

    const EDGES_SOURCENODEID = "sourceNodeId";

    const EDGES_TARGETNODEID = "targetNodeId";

    /**
     * 节点属性名称
     */
    const ATTR_PROPERTIES = "properties";
    const ATTR_NAME = "id";
    const ATTR_DISPLAYNAME = "display_name";
    const ATTR_INSTANCEURL = "instance_url";
    const ATTR_INSTANCENOCLASS = "instance_no_class";
    const ATTR_EXPR = "expr";
    const ATTR_HANDLECLASS = "handle_class";
    const ATTR_FORM = "form";
    const ATTR_FIELD = "field";
    const ATTR_VALUE = "value";
    const ATTR_ATTR = "attr";
    const ATTR_TYPE = "type";
    const ATTR_SCOPE = "scope";
    const ATTR_ASSIGNEE = "assignee";
    const ATTR_ASSIGNEE_HANDLER = "assignment_handler";
    const ATTR_PERFORMTYPE = "perform_type";
    const ATTR_TASKTYPE = "task_type";
    const ATTR_TO = "to";
    const ATTR_PROCESSNAME = "process_name";
    const ATTR_VERSION = "version";
    const ATTR_EXPIRETIME = "expire_time";
    const ATTR_AUTOEXECUTE = "auto_execute";
    const ATTR_CALLBACK = "callback";
    const ATTR_REMINDERTIME = "reminder_time";
    const ATTR_REMINDERREPEAT = "reminder_repeat";
    const ATTR_CLAZZ = "clazz";
    const ATTR_METHODNAME = "method_name";
    const ATTR_ARGS = "args";
    const ATTR_VAR = "var";
    const ATTR_LAYOUT = "layout";
    const ATTR_G = "g";
    const ATTR_OFFSET = "offset";
    const ATTR_PREINTERCEPTORS = "pre_interceptors";
    const ATTR_POSTINTERCEPTORS = "post_interceptors";

    /**
     * 节点元素解析方法，由实现类完成解析
     *
     * @param \lflow\model\ProcessModel $model
     * @param object|array|null         $nodes
     *
     * @return void
     */
    public function parse(ProcessModel $model, null|object|array $nodes): void;

    /**
     * 解析成功后，提供返回NodeModel模型对象
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    public function getCkpt(): NodeCkpt;
}
