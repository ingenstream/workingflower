<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\util;

use ReflectionClass;

/**
 * Class 实列化工具
 *
 * @author Mr.April
 * @since  1.0
 */
class ClassUtil
{
    /**
     * 路径正反斜杠
     *
     * @param $className
     *
     * @return mixed|string[]|null
     */
    public static function instantiateClass($className): mixed
    {
        try {
            $count  = 1;
            $classZ = str_replace('/', '\\', $className, $count);
            if (class_exists($classZ)) {
                return new $classZ();
            } else {
                return null;
            }
        } catch (\Exception $e) {
            Logger::error("Error occurred while instantiating the class: " . $className);
            return null;
        }
    }
}
