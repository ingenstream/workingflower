<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\util;

use think\facade\Config;

/**
 * 配置属性帮助类
 *
 * @author Mr.April
 * @since  1.0
 */
class ConfigHelper
{
    const  PROPERTIES_FILENAME = "lflow";

    /**
     * 根据KEY 获取配置
     *
     * @param string $key
     *
     * @return string|null
     */
    public static function getProperty(string $key): ?string
    {
        if ($key == null) {
            return null;
        }
        return Config::get(self::PROPERTIES_FILENAME . '.' . $key);
    }
}
