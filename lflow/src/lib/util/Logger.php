<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\util;

use think\facade\Log;

/**
 *
 * 日志处理类
 * @author Mr.April
 * @since  1.0
 */
class Logger
{
    public static function isDebugEnabled(): bool
    {
        return true;
    }

    public static function isInfoEnabled(): bool
    {
        return true;
    }

    public static function isNoticEnabled(): bool
    {
        return true;
    }

    public static function isWarningEnabled(): bool
    {
        return true;
    }

    public static function isErrorEnabled(): bool
    {
        return true;
    }

    public static function isCriticalEnabled(): bool
    {
        return true;
    }

    public static function isAlertEnabled(): bool
    {
        return true;
    }

    public static function isEmergencyEnabled(): bool
    {
        return true;
    }

    public static function isSqlEnabled(): bool
    {
        return true;
    }

    public static function debug(string $msg): void
    {
        $backtrace = debug_backtrace();
        $target    = json_encode(array_shift($backtrace));
        self::read($msg . $target, 'debug');
    }

    public static function info(string $msg): void
    {
        self::read($msg, 'info');
    }

    public static function notice(string $msg): void
    {
        self::read($msg, 'notice');
    }

    public static function warning(string $msg): void
    {
        self::read($msg, 'warning');
    }

    public static function error(string $msg): void
    {
        self::read($msg, 'error');
    }

    public static function critical(string $msg): void
    {
        self::read($msg, 'critical');
    }

    public static function alert(string $msg): void
    {
        self::read($msg, 'alert');
    }

    public static function emergency(string $msg): void
    {
        self::read($msg, 'emergency');
    }

    public static function sql(string $msg): void
    {
        self::read($msg, 'sql');
    }

    /**
     * @param string $msg
     * @param string $rank
     */
    public static function read(string $msg = '', string $rank = 'info'): void
    {
        Log::write($msg, $rank);
    }
}
