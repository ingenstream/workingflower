<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\util;

use DateTime;

/**
 * 日期帮助类
 *
 * @author Mr.April
 * @since  1.0
 */
class DateHelper
{
    const DATE_FORMAT_DEFAULT = "Y-m-d H:i:s";

    /**
     * 返回标准格式的当前时间
     *
     * @return string|int
     */
    public static function getTime(): string|int
    {
//        return date(self::DATE_FORMAT_DEFAULT, time());
        return time();
    }

    /**
     * 解析日期时间对象
     *
     * @param object $date
     *
     * @return string
     */
    public static function parseTime(object $date): string
    {
        if ($date == null) return "";
        if ($date instanceof (new DateTime())) {

            return $date->format(self::DATE_FORMAT_DEFAULT);
        }
        return "";
    }

    /**
     * 对时限数据进行处理
     * 1、运行时设置的date型数据直接返回
     * 2、模型设置的需要特殊转换成date类型
     * 3、运行时设置的转换为date型
     *
     * @param string|object $args      运行时参数
     * @param string        $parameter 模型参数
     *
     * @return \DateTime|void|null
     */
    public static function processTime(string|object $args, string $parameter)
    {
        if (StringHelper::isEmpty($parameter)) return null;
        $data = $args->get($parameter);
        if ($data == null) $data = $parameter;
        if ($data instanceof (new DateTime())) {
            return $data;
        }
    }

    /**
     * 格式化字符串时间
     *
     * @param string|null $str
     *
     * @return string|null
     */
    public static function format(string|null $str)
    {
        if (StringHelper::isEmpty($str)) return null;
        return date(self::DATE_FORMAT_DEFAULT, strtotime($str));
    }
}
