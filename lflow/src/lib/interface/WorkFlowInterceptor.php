<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\interface;

use lflow\core\Execution;

/**
 * 任务拦截器，对产生任务的结果进行拦截
 *
 * @author Mr.April
 * @since  1.0
 */
interface WorkFlowInterceptor

{
    /**
     * 拦截方法，参数为执行对象
     *
     * @param \lflow\core\Execution $execution 执行对象。可从中获取执行的数据
     */
    public function intercept(Execution $execution): void;

}
