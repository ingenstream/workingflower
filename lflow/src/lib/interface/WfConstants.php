<?php

namespace lflow\lib\interface;

interface WfConstants
{
    public const ORDER_STATE_KEY = "order_state"; // 自定义实例状态key
    public const ORDER_USER_REAL_NAME_KEY = "operator_real_name"; // 用户姓名key
    public const ORDER_USER_NAME_KEY = "operator_user_name"; // 用户名key
    public const ORDER_USER_ID = "operator_user_id"; // 用户名key
    public const APPROVAL_TYPE = "approval_type"; // 类型
    public const APPROVAL_OPINION = "approval_opinion";//审批意见
    public const REMARK = "remark"; // 原因
    public const TARGET_NODE_NAME = "target_node_name"; // 目地节点名称，用于退回到指定节点的
    public const FIRST_TASK_NAME = "apply";
    public const INSTANCE_URL = "instance_url"; // 实例化地址-表单key
}
