<?php

namespace lflow\lib\enum;

class WfOrderStateEnum extends \PhpEnum\Enum
{

    /**
     * 已完成
     */
    const FINISHED = [0, "已完成"];

    /**
     * 进行中
     */
    const DOING = [1, "进行中"];

    /**
     * 强制中止
     */
    const TERMINATE = [2, "强制中止"];

    /**
     * 不同意(强制中止)
     */
    const DISAGREE = [3, "不同意(强制中止)"];

    /**
     * 取回流程(强制中止)
     */
    const TAKE_BACK = [4, "取回流程(强制中止)"];

    /**
     * 作废流程(强制中止)
     */
    const CANCEL = [5, "作废流程(强制中止)"];

    /**
     * 驳回流程(强制中止)
     */
    const REJECT = [6, "驳回流程(强制中止)"];

    private $id;
    private $name;

    protected function construct($id, $name)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

}
