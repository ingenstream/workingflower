<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\impl;

use lflow\core\Execution;
use lflow\lib\util\Logger;
use lflow\lib\interface\WorkFlowInterceptor;

/**
 * 日志拦截器
 *
 * @author Mr.April
 * @since  1.0
 */
class LogInterceptor implements WorkFlowInterceptor
{

    /**
     * 拦截产生的任务对象，打印日志
     *
     * @param \lflow\core\Execution $execution
     */
    public function intercept(Execution $execution): void
    {
        if (Logger::isInfoEnabled()) {
            foreach ($execution->getTasks() as $task) {
                $buffer = "创建任务[标识=" . $task->getData('id');
                $buffer .= ",名称=" . $task->getData('display_name');
                $buffer .= ",创建时间=" . $task->getData('create_time');
                $buffer .= ",参与者={";
                if (!empty($task->getData('actor_ids'))) {
                    foreach ($task->getData('actor_ids') as $actor) {
                        $buffer .= $actor . ';';
                    }
                    $buffer .= "}]";
                }
                Logger::info($buffer);
            }
        }
    }
}
