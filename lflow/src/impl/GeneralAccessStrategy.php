<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\impl;


use lflow\lib\util\ArrayHelper;

/**
 * 基于用户或组（角色、部门等）的访问策略类
 * 该策略类适合组作为参与者的情况
 *
 * @author Mr.April
 * @since  1.0
 */
class GeneralAccessStrategy
{
    /**
     * 如果操作人id所属的组只要有一项存在于参与者集合中，则表示可访问
     *
     * @param string $operator
     * @param object $actors
     *
     * @return bool
     * @throws \lflow\WorkFlowException
     */
    public static function isAllowed(string $operator, object $actors): bool
    {
        $isAllowed = false;
        foreach ($actors as $actor) {
            $actor = ArrayHelper::arrayToSimpleObj($actor, TaskActor::class);
            if (StringHelper::equalsIgnoreCase($actor->getActorId(), $operator)) {
                $isAllowed = true;
                break;
            }
        }
        return $isAllowed;
    }
}
