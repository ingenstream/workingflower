<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\impl;

use lflow\lib\interface\Completion;
use lflow\lib\util\Logger;
use lflow\model\HistoryOrderModel;
use lflow\model\HistoryTaskModel;

/**
 * 默认任务实例完成时触发的动作
 *
 * @author Mr.April
 * @since  1.0
 */
class GeneralCompletion implements Completion
{

    public function complete(HistoryOrderModel|HistoryTaskModel $model): void
    {
        //完成任务
        if ($model instanceof HistoryTaskModel) {
            $text = $model->getData('id') . '  操作人ID:' . $model->getData('operator');
            Logger:: info("The task[{}] has been user[{}] has completed TaskId:" . $text);
        }
        //完成实例
        if ($model instanceof HistoryOrderModel) {
            Logger:: info("The task[{}] has been user[{}] has completed" . json_encode($model->toArray()));
        }
    }

    public function test(){
        dump(123);exit;
    }
}
