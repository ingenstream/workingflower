<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

use lflow\core\Execution;
use lflow\exceptions\WorkFlowException;
use lflow\handlers\IHandler;

/**
 * 基础模型
 *
 * @author Mr.April
 * @since  1.0
 */
class BaseCkpt
{
    /**
     * 元素名称
     */
    public string|null $name;

    /**
     * 显示名称
     */
    public string $displayName;

    public function getName(): string|null
    {
        return $this->name;
    }

    public function setName(string|null $name): void
    {
        $this->name = $name;
    }

    public function getDisplayName(): string|null
    {
        return $this->displayName;
    }

    public function setDisplayName(string|null $displayName): void
    {
        $this->displayName = $displayName;
    }

    /**
     * 将执行对象execution交给具体的处理器处理
     *
     * @param \lflow\handlers\IHandler $handler
     * @param \lflow\core\Execution    $execution
     */
    protected function fire(IHandler $handler, Execution $execution): void
    {
        $handler->handle($execution);
    }

    /**
     *
     * @param $message
     */
    public function Error($message = null): void
    {
        throw new WorkFlowException($message);
    }
}
