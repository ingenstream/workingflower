<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

use lflow\core\Execution;

/**
 * 分支定义fork元素
 *
 * @author Mr.April
 * @since  1.0
 */
class ForkCkpt extends NodeCkpt
{

    public function exec(Execution $execution): void
    {
        $this->runOutTransition($execution);
    }
}
