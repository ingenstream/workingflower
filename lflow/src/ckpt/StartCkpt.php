<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;


use lflow\core\Execution;

/**
 * 开始节点定义start元素
 *
 * @author Mr.April
 * @since  1.0
 */
class StartCkpt extends NodeCkpt
{

    /**
     * 开始节点无输入变迁
     *
     * @return array
     */
    public function getInputs(): array
    {
        return [];
    }

    public function exec(Execution $execution): void
    {
        $this->runOutTransition($execution);
    }

}
