<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

/**
 * 字段模型类
 *
 * @author Mr.April
 * @since  1.0
 */
class FieldCkpt extends BaseCkpt
{

    /**
     * 字段类型
     */
    private string $types;

    /**
     * 字段模型对应的属性key/value数据
     */
    public object $attrMap;

    public function __construct()
    {
        $this->attrMap = (object)[];
    }

    public function setTypes(string $type): void
    {
        $this->types = $type;
    }

    public function getTypes(): string
    {
        return $this->types;
    }

    /**
     * 向属性集合添加key/value数据
     *
     * @param string $name  名称
     * @param string $value 属性值
     */
    public function addAttr(string $name, string $value)
    {
        $this->attrMap->{$name} = $value;
    }

    /**
     * 获取属性集合
     *
     * @return object 属性集合
     */
    public function getAttrMap(): object
    {
        return $this->attrMap;
    }

    /**
     * 设置属性集合
     *
     * @param object $attrMap 属性集合
     */
    public function setAttrMap(object $attrMap): void
    {
        $this->attrMap = $attrMap;
    }

    public function toString(): string
    {
        return json_encode(get_object_vars($this));

    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

}
