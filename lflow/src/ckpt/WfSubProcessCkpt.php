<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

use lflow\core\Execution;

/**
 * 子流程subprocess元素
 *
 * @author Mr.April
 * @since  1.0
 */
class WfSubProcessCkpt extends WorkFlowCkpt
{
    /**
     * 子流程名称
     */
    private string $processName;

    /**
     * 子流程版本号
     */
    private int $version;

    /**
     * 子流程定义引用
     */
    private ProcessCkpt $subProcess;


    public function exec(Execution $execution): void
    {
        $this->runOutTransition($execution);
    }

    public function getSubProcess(): ProcessCkpt
    {
        return $this->subProcess;
    }

    public function setSubProcess(ProcessCkpt $subProcess): void
    {
        $this->subProcess = $subProcess;
    }

    public function getProcessName(): string
    {
        return $this->processName;
    }

    public function setProcessName(string $processName): void
    {
        $this->processName = $processName;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

}
