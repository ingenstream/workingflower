<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\core;

use lflow\core\services\CcOrderServices;
use lflow\core\services\HistoryOrderServices;
use lflow\core\services\HistoryTaskActorServices;
use lflow\core\services\HistoryTaskServices;
use lflow\core\services\OrderServices;
use lflow\core\services\TaskActorServices;
use lflow\core\services\TaskServices;
use lflow\lib\util\AssertHelper;
use lflow\model\HistoryOrderModel;
use lflow\model\OrderModel;
use lflow\model\TaskModel;
use think\facade\Db;

class QueryServices extends BaseServices
{

    /**
     * @param string $orderId
     *
     * @return \lflow\model\OrderModel|null
     */
    public function getOrder(string $orderId): ?OrderModel
    {
        $orderServices = app()->make(OrderServices::class);
        return $orderServices->get($orderId, ['*']);
    }

    /**
     * @param string $taskId
     *
     * @return \lflow\model\TaskModel|null
     */
    public function getTask(string $taskId): ?TaskModel
    {
        $taskServices = app()->make(TaskServices::class);
        return $taskServices->get($taskId, ['*']);
    }

    /**
     * @param string $taskId
     *
     * @return array
     */
    public function getTaskActorsByTaskId(string $taskId): array
    {
        $taskActorServices = app()->make(TaskActorServices::class);
        return $taskActorServices->getColumn(['task_id' => $taskId], 'actor_id');
    }

    /**
     * @param string $taskId
     *
     * @return array
     */
    public function getHistoryTaskActorsByTaskId(string $taskId): array
    {
        $historyTaskActorServices = app()->make(HistoryTaskActorServices::class);
        return $historyTaskActorServices->getColumn(['task_id' => $taskId], 'actor_id');
    }

    /**
     * @param string $orderId
     *
     * @return \lflow\model\HistoryOrderModel|null
     */
    public function getHistOrder(string $orderId): ?HistoryOrderModel
    {
        $historyOrderServices = app()->make(HistoryOrderServices::class);
        return $historyOrderServices->get($orderId, ['*']);
    }

    /**
     * @param string $orderId
     *
     * @return \lflow\model\HistoryOrderModel|null
     */
    public function getHistOrders(string $orderId): ?HistoryOrderModel
    {
        AssertHelper::notNull($orderId);
        $historyOrderServices = app()->make(HistoryOrderServices::class);
        return $historyOrderServices->dao->getInfo(['id' => $orderId], '*', ['processs'], true);
    }

    /**
     * 获取历史实例
     *
     * @param array  $where
     * @param string $order
     * @param bool   $isPage
     *
     * @return array
     */
    public function getHistoryOrders(array $where = [], string $order = "create_time desc", bool $isPage = false): array
    {
        [$page, $limit] = $this->getPageValue();
        AssertHelper::notNull($where['creator'] ?? '', '[creator] - this argument is required; it must not be null');
        $HistoryOrderServices = app()->make(HistoryOrderServices::class);
        $list                 = $HistoryOrderServices->getOrders($where, '*', $page, $limit, $order, ['processs'], true);
        $count                = $HistoryOrderServices->getOrdersCount($where, ['processs'], true);
        return compact('list', 'count');
    }

    /**
     * @param array  $where
     * @param string $order
     * @param bool   $isPage
     *
     * @return array
     */
    public function getHistoryTasks(array $where = [], string $order = "finish_time desc", bool $isPage = false): array
    {
        AssertHelper::notNull($where);
        [$page, $limit] = $this->getPageValue($isPage);
        $historyTaskServices = app()->make(HistoryTaskServices::class);
        $list                = $historyTaskServices->dao->getHistoryTask($where, '*', $page, $limit, $order, ['taskActor'], true);
        $count               = $historyTaskServices->dao->getHistoryTaskCount($where, ['taskActor'], true);
        return compact('list', 'count');
    }

    /**
     * @param array $where
     * @param bool  $isPage
     *
     * @return array
     */
    public function getActiveTasks(array $where = [], bool $isPage = false): array
    {
        AssertHelper::notNull($where);
        [$page, $limit] = $this->getPageValue($isPage);
        $taskServices = app()->make(TaskServices::class);
        $list         = $taskServices->dao->getTask($where, '*', $page, $limit, '', ['taskActor'], true);
        $count        = $taskServices->getTaskCount($where, ['taskActor'], true);
        return compact('list', 'count');
    }

    /**
     * @param array $where
     *
     * @return mixed
     */
    public function getActiveOrder(array $where = []): array
    {
        AssertHelper::notNull($where);
        $orderServices = app()->make(OrderServices::class);
        return $orderServices->dao->getOrders($where, '*', 0, 0, '', [], true);
    }

    /**
     * 我的待办列表
     *
     * @param array $where
     * @param bool  $isPage
     *
     * @return array
     */
    public function getWorkItems(array $where, bool $isPage = false): array
    {
        AssertHelper::notNull($where ?? '', "[Assertion where] - this argument is required; it must not be null");
        [$page, $limit] = $this->getPageValue($isPage);
        $taskServices = app()->make(TaskServices::class);
        $list         = $taskServices->dao->getTask($where, '*', $page, $limit, '', ['orders', 'processs', 'taskActor'], true);
        $count        = $taskServices->getTaskCount($where, ['orders', 'processs', 'taskActor'], true);
        return compact('list', 'count');
    }

    public function getHistoryWorkItems(array $where, bool $isPage = false): array
    {
        AssertHelper::notNull($where ?? '', "[Assertion where] - this argument is required; it must not be null");
        [$page, $limit] = $this->getPageValue($isPage);
        $historyTaskServices = app()->make(HistoryTaskServices::class);
        $list                = $historyTaskServices->dao->getHistoryTask($where, '*', $page, $limit, '', ['orders', 'processs', 'taskActor'], true);
        $count               = $historyTaskServices->getHistoryTaskCount($where, ['orders', 'processs', 'taskActor'], true);
        return compact('list', 'count');
    }

    /**
     * 获取任务详情
     *
     * @param array $where
     *
     * @return mixed
     */
    public function getTaskDetails(array $where): mixed
    {
        AssertHelper::notNull($where, "[Assertion where] - this argument is required; it must not be null");
        $taskServices = app()->make(TaskServices::class);
        return $taskServices->dao->getInfo($where, '*', ['orders', 'processs', 'taskActor'], true);
    }

    public function getHistoryTaskDetails(array $where)
    {
        AssertHelper::notNull($where, "[Assertion where] - this argument is required; it must not be null");
        $historyTaskServices = app()->make(HistoryTaskServices::class);
        return $historyTaskServices->dao->getInfo($where, '*', ['orders', 'processs', 'taskActor'], true);
    }

    public function getCCOrder(string $orderId)
    {
        $historyTaskServices = app()->make(CcOrderServices::class);
        return $historyTaskServices->dao->selectList(['order_id' => $orderId], '*', 0, 0, '', [], true);
    }

    /**
     * 原生查询构造器
     *
     * @param string       $sql
     * @param array|string $args
     * @param bool         $isLib
     *
     * @return mixed
     */
    public function nativeQuery(string $sql, array|string $args, bool $isLib = false): mixed
    {
        AssertHelper::notEmpty($sql);
        AssertHelper:: notNull($args);
        return Db::query($sql, $args, $isLib);
    }
}
