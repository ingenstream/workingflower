<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\core\services;

use lflow\core\BaseServices;
use lflow\dao\TaskActorDao;

class TaskActorServices extends BaseServices
{

    /**
     * @param \lflow\dao\TaskActorDao $dao
     */
    public function __construct(TaskActorDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 移除指定处理人
     * @param string       $taskId
     * @param string|array $actors
     *
     * @return void
     */
    public function removeTaskActor(string $taskId, string|array $actors): void
    {
        $actors = is_array($actors) ? $actors : explode(',', $actors);
        foreach ($actors as $actorId) {
            $where = [['task_id', '=', $taskId], ['actor_id', '=', $actorId]];
            $this->dao->delete($where);
        }
    }

}
