<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\scheduling;

use lflow\entity\Process;
use lflow\model\NodeModel;

/**
 * 提醒接口
 *
 * @author GQL
 * @since  1.0
 */
interface IReminder
{

    /**
     * 提醒操作
     *
     * @param \lflow\entity\Process  $process   流程定义对象
     * @param String                 $orderId   流程实例id
     * @param String                 $taskId    任务id
     * @param \lflow\model\NodeModel $nodeModel 节点模型
     * @param object|null            $data      数据
     *
     * @return mixed
     */
    public function remind(Process $process, string $orderId, string $taskId, NodeModel $nodeModel, ?object $data);

}