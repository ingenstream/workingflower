<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\scheduling;

/**
 * 任务执行后回调
 *
 * @author GQL
 * @since  1.0
 */
interface JobCallback
{
    /**
     * 回调函数
     *
     * @param string $taskId   当前任务id
     * @param array  $newTasks 新产生的任务集合
     *
     * @return mixed
     */
    public function callback(string $taskId, array $newTasks);

}